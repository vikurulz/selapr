package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;


public class HomePage extends Annotations{ 

	/*public HomePage() {
       PageFactory.initElements(driver, this);
	} 
*/
//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	public HomePage verifyLoginName(String data) {
		WebElement eleHomeTitle = locateElement("tagname", "h2");
		String loginName = eleHomeTitle.getText();
		if(loginName.contains(data)) {
			System.out.println("Login success");
		}else {
			System.out.println("Logged username mismatch");
		}
		return this;
	}
	
	public MyHomePage clickCRMSFAButton() {
		WebElement eleCrmSfa = locateElement("link", "CRM/SFA");
		click(eleCrmSfa);
		return new MyHomePage();

}
}







