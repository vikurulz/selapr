package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
		public CreateLeadPage enterCompName(String data) {
			WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompName, data);
		return this;
	}
		
		public CreateLeadPage enterFirstName(String data) {
			WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
			clearAndType(eleFirstName, data);
			return this;
		}
		public CreateLeadPage enterLastName(String data) {
			WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
			clearAndType(eleLastName, data);
			return this;
		}
		public ViewLeadPage clickCreateLeadButton() {
			WebElement eleClickCreateLead = locateElement("xpath", "//input[@value='Create Lead']");
			click(eleClickCreateLead);
			return new ViewLeadPage();
			
		}
	
	
	
	
	
	

}
