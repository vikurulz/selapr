package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC001_LoginAndLogout extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_LoginAndLogout";
		testcaseDec = "Create leads";
		author = "vigneshwaran";
		category = "smoke";
		excelFileName = "TC001";
	
	} 
	

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String userName, String password, String logInName,String cName, String fName, String lName) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassWord(password) 
		.clickLogin()
		.clickCRMSFAButton()
		.clickLeadsButton()
		.clickCreateLeadsButton()
		.enterCompName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLeadButton()
		.verifyFirstName(fName);
	}
	
}






